# Doku https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database
#      https://cloud.google.com/sql/docs/postgres/instance-settings
#      https://cloud.google.com/sql/docs/postgres/connect-overview
#      https://github.com/terraform-google-modules/terraform-google-sql-db/tree/master/modules/postgresql


#activate  Compute Engine API mus be enabled in the GCP
resource "google_project_service" "gcp_ce_api" {
  depends_on         = [google_project_service.gcp_general_apis]
  service            = "compute.googleapis.com"
  disable_on_destroy = false
}

resource "google_service_account" "default" {
  account_id   = "service_account_id"
  display_name = "Service Account"
}

resource "google_compute_instance" "default" {
  name         = "${var.resource_name_prefix}-${var.leanix_id}-${var.service_name}-compute-engine-${terraform.workspace}-${random_string.random_suffix.result}"
  machine_type = var.machine_type
  zone         = var.zone
  region       = var.region

  tags = ["test", "geigerale"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "echo hi > /test.txt"
}