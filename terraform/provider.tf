provider "google" {
  project = var.project
}

terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
  required_version = ">= 0.13"
}


#activate gcp service api
resource "google_project_service" "gcp_general_apis" {
  for_each = toset(["cloudresourcemanager.googleapis.com",  
                     "runtimeconfig.googleapis.com",
                      "serviceusage.googleapis.com",
                      "containerregistry.googleapis.com"])
  service            = each.key
  disable_on_destroy = false
}

# create a random suffix
resource "random_string" "random_suffix" {
  length  = 3
  special = false
  lower   = true
  upper   = false
}