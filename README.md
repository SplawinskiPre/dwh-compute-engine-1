# Template to create a Cloud SQL Database

## Content

* [Intro](#intro)
* [Prerequesites](#prerequisites)
* [Repo Setup](#repo-setup)
* [Terraform folder](#terraform_folder)
* [How to use this repository](#how_to_use_this_repository)
* [How to access to the database from local Computer](#how_to_access_to_the_database_from_local_computer)
---

# Intro

The idea of this repository is to provide a template to create Cloud SQL Database(s) and grant permission in your project.
All resources are provided directly within this repository.

---

# Prerequisites

* GCP account (Your Hermes-AD credentials)
* Needed service account permissions: Service Account User, Cloud SQL Admin, Storage Object Admin, Service Account Key Admin https://cloud.google.com/iam/docs/understanding-roles
* Needed APIs: sqladmin.googleapis.com, cloudresourcemanager.googleapis.com, runtimeconfig.googleapis.com, serviceusage.googleapis.com
* Bitbucket Repo
* Bucket in the Cloud Storage (for using the Terraform scripts)


---

# Repo Setup

**File** | **Description**
--- | ---
.gitignore | Probably pretty self-explanatory for any developer.
bitbucket-pipelines.yml | Bitbucket Pipeline
README.md | This file.
/terraform	| See below for detailed file explanation.
---

# Terraform folder

**File** | **Description**
--- | ---
backend.tf	Contains the definition for your Terraform Bucket
postgres.tf	Contains all the needed steps to generate a PostgreSQL-ready Google SQL instance in your project.
provider.tf	Contains some basic boilerplate code to tell Terraform that we work with Google Cloud.
variable.common.auto.tfvars	Holds the most basic variables and will set those as defaults for all stages.
enviroment/variable.dev.tfvars	Holds all variables that are specific for your development environment.
enviroment/variable.int.tfvars	Holds all variables that are specific for your integration environment.
enviroment/variable.prd.tfvars	Holds all variables that are specific for your production environment.
variables.tf	Defines all the available variables and their "factory" defaults.
---

# How to use this repository

1. Fork *THIS* (cloudsql) repo to your bitbucket project
2. Clone the forked repo on your PC
3. Update the variables in `backend.tf`,`variable.common.auto.tfvars`,`variable.dev.tfvars`,`variable.int.tfvars`,`variable.prd.tfvars` according to your needs, here is an overview over the roles ( https://cloud.google.com/iam/docs/understanding-roles )
4. Upload everything to your Bitbucket Cloud Repository.
5. Set the following "Repository variables": *GOOGLE_REGISTRY_CREDENTIALS_BASE64* and *GOOGLE_REGISTRY_CREDENTIALS_BASE64*: The base64-encoded credentials for your GCP Service Account (cat gcp_key.json | tr -d '\n' | base64 -w0 > mein_base64_ergebnis.txt).   
6. Let the pipeline run!
---

## How to access to the database from local Computer
1. You need the Cloud SQL Auth-Proxy, the proxy establishes the connection to the database.   
   Can be download here https://github.com/GoogleCloudPlatform/cloudsql-proxy/releases
2. After download, start the Cloud SQL Auth-Proxy in a terminal with following parameter.  
   *cloud_sql_proxy* -credential_file='**my-service-accunt-credential.json**' -instances=**my-database-connection-name**=tcp:**local-port-from-proxy**  
   example: *cloud_sql_proxy* -credential_file='C:\keys\hg-p1234-dummy-npr-0e4a-9a524f03f2e6.json' -instances=hg-p1234-dummy-npr-0e4a:europe-west1:hg-p1234-dummy-cloudsql-inst-dev-v5f=tcp:8080  
   You can find the (my-database-)**connection name** in the database instance overview under 'Connect to this instance'  
3. Configure your local SQL Client. The databse cccess is via the Cloud SQL Auth Proxy.  
